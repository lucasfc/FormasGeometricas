#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <Shape.h>


class Triangle : public Shape
{
    public:

        Triangle();
        float GetArea();

    protected:

    private:

};

#endif // TRIANGLE_H
