#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <Shape.h>


class Rectangle : public Shape
{
    public:
    Rectangle();
    float GetArea();

    protected:

    private:

};

#endif // TRIANGLE_H
