#ifndef SQUARE_H
#define SQUARE_H

#include <Shape.h>


class Square : public Shape
{
    public:
    Square();
    float GetArea();
    protected:

    private:

};

#endif // SQUARE_H
