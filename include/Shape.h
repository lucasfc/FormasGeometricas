#ifndef SHAPE_H
#define SHAPE_H


class Shape
{

    public:
        float GetWidth() { return width; }
        void setWidth(float val) { width = val; }
        float GetHeight() { return height; }
        void SetHeight(float val) { height = val; }
        Shape();
    protected:

    private:
        float width;
        float height;
};

#endif // SHAPE_H
